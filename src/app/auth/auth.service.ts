import { Injectable } from '@angular/core';
import { Observable, Subject, bindCallback, of } from 'rxjs';
import { map, skip, concatMap } from 'rxjs/operators';

declare global {
  interface Window {
    FB: any;
    fbAsyncInit: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  fbReady: Subject<boolean> = new Subject<boolean>();
  constructor() {
    const that = this;

    /* tslint:disable */
    window.fbAsyncInit = function() {
      window.FB.init({
        appId      : '2063886213660916',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.2'
      });

      window.FB.AppEvents.logPageView();
      that.fbReady.next(true);
      that.fbReady.complete();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
     /* tsling:enable */
  }

  login(): void {
    window.FB.login();
  }

  logout(): void {
    window.FB.logout(() => window.location.reload());
  }

  checkLogin(): Observable<boolean> {
    return this.getFbReady().pipe(
      concatMap( () =>
        bindCallback((cb) => window.FB.getLoginStatus(cb))().pipe(
          map((response: any) => response.status === 'connected'),
        )
      )
    );
  }

  private getFbReady(): Observable<boolean> {
    return this.fbReady.isStopped ? of(true) : this.fbReady;
  }
}
