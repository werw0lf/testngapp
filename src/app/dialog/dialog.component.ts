import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  constructor(public dialogRef: MatDialogRef<DialogComponent>, private authService: AuthService) { }

  close(): void {
    this.dialogRef.close();
  }

  login(): void {
    console.log('login');
    this.authService.login();
    this.dialogRef.close();
  }
}
